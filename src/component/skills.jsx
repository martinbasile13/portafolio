


export default function Skills() {
    return (
        <div className="flex flex-col items-center justify-center">
            <h1 className="text-2xl m-3">Front End</h1>
            <div className="flex flex-col items-start justify-start">
                <div className="flex flex-wrap">
                    <kbd className="kbd kbd-sm mx-2 px-2"><img src="../../public/icons/html.svg" alt="" /> Html5 </kbd>
                    <kbd className="kbd kbd-sm mx-2 px-2"><img src="../../public/icons/css.svg" alt="" /> CSS </kbd>
                    <kbd className="kbd kbd-sm mx-2 px-2"><img src="../../public/icons/tailwind.svg" alt="" /> TailwindCSS </kbd>
                </div>
                <h1 className="m-2 text-lg">Lenguajes</h1>
                <div className="flex flex-wrap">
                    <kbd className="kbd kbd-sm mx-2 px-2"><img src="../../public/icons/javascript.svg" alt="" /> JavaScript </kbd>
                    <kbd className="kbd kbd-sm mx-2 px-2"><img src="../../public/icons/typescript.svg" alt="" /> TypeScript </kbd>
                </div>
                <h1 className="m-2 text-lg">Framework</h1>
                <div className="flex flex-wrap">
                    <kbd className="kbd kbd-sm mx-2 px-2"><img src="../../public/icons/react.svg" alt="" /> React JS </kbd>
                </div>
            </div>
            <h1 className="text-2xl m-3">Back End</h1>
            <div className="flex flex-col items-start justify-start">
                <div className="flex flex-wrap">
                    <kbd className="kbd kbd-sm mx-2 px-2"><img src="../../public/icons/git.svg" alt="" /> Git </kbd>
                    <kbd className="kbd kbd-sm mx-2 px-2"><img src="../../public/icons/bash.svg" alt="" /> Bash </kbd>
                    <kbd className="kbd kbd-sm mx-2 px-2"><img src="../../public/icons/docker.svg" alt="" /> Docker </kbd>
                </div>
                <h1 className="m-2 text-lg">Bases de datos</h1>
                <div className="flex flex-wrap">
                    <kbd className="kbd kbd-sm mx-2 px-2"><img src="../../public/icons/mongodb.svg" alt="" /> MongoDB </kbd>
                    <kbd className="kbd kbd-sm mx-2 px-2"><img src="../../public/icons/mysql.svg" alt="" /> MySql </kbd>
                    <kbd className="kbd kbd-sm mx-2 px-2"><img src="../../public/icons/postgresql.svg" alt="" /> PostgreSql </kbd>
                </div>
                <h1 className="m-2 text-lg">Lenguajes</h1>
                <div className="flex flex-wrap">
                    <kbd className="kbd kbd-sm mx-2 px-2"><img src="../../public/icons/python.svg" alt="" /> python </kbd>
                    <kbd className="kbd kbd-sm mx-2 px-2"><img src="../../public/icons/javascript.svg" alt="" /> JavaScript </kbd>
                </div>
                <h1 className="m-2 text-lg">Framework</h1>
                <div className="flex flex-wrap">
                    <kbd className="kbd kbd-sm mx-2 px-2"><img src="../../public/icons/django.svg" alt="" /></kbd>
                    <kbd className="kbd kbd-sm mx-2 px-2"><img src="../../public/icons/nodejs.svg" alt="" /></kbd>
                </div>
            </div>
        </div>
    )
}